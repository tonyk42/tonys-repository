## Custumer and private/public repositories
A customer for capstone project may be interested in learning about similar projects. Public repositories are good for sharing information. 
However, they may choose to make the capstone project private because of security issues. The customer may not want anyone else to have access to
proprietary information.

## Customer and Private Signal Messenger
The customer may be looking for a messenger service with very high security. Private Signal Messenger would meet their needs in this way.
Moreover, it is open-source. Others can see the positive aspects and make things better.