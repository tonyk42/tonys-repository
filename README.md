## Instructions on how to build, install, and use repetition.js
---
1. The user will require Visual Studio Code or another IDE that can use Javascript files
2. The IDE should also have a terminal that can run prompts
3. The user should open repetition.js
4. The user should run terminal by using F5 or by clicking on terminal
5. This program assumes that node.js is installed. 
6. Instructions on installing node.js can be found at https://nodejs.org/
7. The user presses F5 to run the program.


## License information
---

The MIT license was chosen because it is a permissive free license.

This is a practice change.

